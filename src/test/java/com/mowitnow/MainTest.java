package com.mowitnow;

import junit.framework.TestCase;

public class MainTest extends TestCase {
    private Lawn lawn = new Lawn(5,5);

    public void testMower1() throws Exception {

        Mower myMower1 = new Mower();
        myMower1.setPosition(new Position(1,2));
        myMower1.setOrientation(Orientation.N);
        myMower1.putOn(lawn);
        String actions1 = "GAGAGAGAA";

        Position finalPosition = myMower1.execute(actions1);

        assertEquals(finalPosition, new Position(1,3));
        assertEquals(myMower1.getOrientation(), Orientation.N);
    }

    public void testMower2() throws Exception{
        Mower myMower2 = new Mower();
        myMower2.setPosition(new Position(3,3));
        myMower2.setOrientation(Orientation.E);
        myMower2.putOn(lawn);
        String actions2 = "AADAADADDA";

        Position finalPosition = myMower2.execute(actions2);

        assertEquals(finalPosition, new Position(5,1));
        assertEquals(myMower2.getOrientation(), Orientation.E);
    }
}