package com.mowitnow;
/**
 *This Enumeration represent the different orientation can have mower.
 */
enum Orientation {N, W, S, E}