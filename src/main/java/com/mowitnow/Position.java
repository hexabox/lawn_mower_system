package com.mowitnow;
/**
 *Represents a 2D position with x, y on lawn surface.
 */
class Position{
    public int x;
    public int y;
    public Position(int x, int y){
        this.x = x;
        this.y = y;
    }
    public boolean equals(Object position2) {
        return position2 instanceof Position &
                ((Position)position2).x == this.x && ((Position)position2).y == this.y;
    }
    public void printPoint(){
        System.out.printf("(%s, %s)", this.x, this.y);
    }
}