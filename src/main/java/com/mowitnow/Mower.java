package com.mowitnow;

/**
 *Represent a mower object.
 */
class Mower {
    private Position position;
    private Lawn lawn;
    private Orientation orientation;

    public Mower(Position position, Orientation orientation) {
        this.position = position;
        this.orientation = orientation;
    }

    public Mower() {
        this(new Position(0, 0), Orientation.N);
    }

    public void setPosition(Position position) throws Exception {
        if(this.lawn != null && !this.lawn.isCorrectPosition(position))
            throw new Exception("Sorry but start position is incorrect");
        this.position = position;
    }

    public Position getPosition() {
        return this.position;
    }

    public void setOrientation(Orientation orientation){
        this.orientation = orientation;
    }

    public Orientation getOrientation(){
        return this.orientation ;
    }

    public void printPosition() {
        System.out.printf("The final position of the mower is at X: %d, Y: %d, Orientation: %s\n",
                this.position.x,
                this.position.y,
                this.orientation);
    }

    /**
     *Puts a lawn mower on a lawn.
     */
    public void putOn(Lawn lawn) throws Exception {

        if (!lawn.isCorrectPosition(this.position))
            throw new Exception("Sorry but start position is incorrect");

        this.lawn = lawn;

    }
    /**
     *Execute a series of instructions that the mower must perform.
     */
    public Position execute(String actions){
        for (char a: actions.toLowerCase().toCharArray())
        {
            if (a == 'a')
                ExecAction(Action.A);
            if (a == 'd')
                ExecAction(Action.D);
            if (a == 'g')
                ExecAction(Action.G);
        }
        return this.position;
    }

    private void ExecAction(Action action) {
        //Change Orientation if we have a G action
        if (action == Action.G) {
            switch (this.orientation) {
                case N:
                    this.orientation = Orientation.W;
                    break;
                case W:
                    this.orientation = Orientation.S;
                    break;
                case S:
                    this.orientation = Orientation.E;
                    break;
                case E:
                    this.orientation = Orientation.N;
                    break;
            }
        } else if (action == Action.D) {
            //Change Orientation if we have a D action
            switch (this.orientation) {
                case N:
                    this.orientation = Orientation.E;
                    break;
                case E:
                    this.orientation = Orientation.S;
                    break;
                case S:
                    this.orientation = Orientation.W;
                    break;
                case W:
                    this.orientation = Orientation.N;
                    break;
            }
        } else if (action == Action.A) {
            //Change Position if we have a A action
            switch (this.orientation) {
                case N:
                    this.position.y = this.position.y + 1;
                    break;
                case E:
                    this.position.x = this.position.x + 1;
                    break;
                case S:
                    this.position.y = this.position.y - 1;
                    break;
                case W:
                    this.position.x = this.position.x - 1;
                    break;
            }
        }
        //If go out of the lawn borders keep it inside
        if (this.position.x < 0)
            this.position.x = 0;
        if (this.position.x > this.lawn.width)
            this.position.x = this.lawn.width;
        if (this.position.y < 0)
            this.position.y = 0;
        if (this.position.y > this.lawn.length)
            this.position.y = this.lawn.length;

    }
}

