package com.mowitnow;
/**
 *Represent the lawn surface with width and length.
 */
class Lawn {
    public int width;       //The x axis
    public int length;      //The y axis

    public Lawn(int width, int length) {
        this.width = width;
        this.length = length;
    }
    /**
     *Check if a position is in the lawn.
     */
    public boolean isCorrectPosition(Position position) {
        if (position.x < 0 || position.y < 0)
            return false;
        if (position.x > this.width || position.y > this.length)
            return false;
        return true;
    }
}