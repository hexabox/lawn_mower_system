package com.mowitnow;

import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws Exception {

        //Read file.
        String fileName = "file.txt";

        Stream<String> stream = Files.lines(Paths.get(fileName));
        Iterator<String> fileIt =  stream.iterator();

        //Steap 1 extract information from file
        //Extract size of Lawn.
        String line1 = fileIt.next();
        Pattern pSize = Pattern.compile("^(\\d+) (\\d+)$");
        Matcher m = pSize.matcher(line1);
        m.find();

        int width = Integer.valueOf(m.group(1));
        int length = Integer.valueOf(m.group(2));

        //Extract position and oriontation of first mower.
        String line2 = fileIt.next();
        Pattern pPosition = Pattern.compile("^(\\d+) (\\d+) ([NWSE])$");
        m = pPosition.matcher(line2);
        m.find();

        int positionX1 = Integer.valueOf(m.group(1));
        int positionY1 = Integer.valueOf(m.group(2));
        Orientation orent1 = orientationFromString(m.group(3));

        //Extract actions of first mower.
        String line3 = fileIt.next();
        Pattern pAction = Pattern.compile("^[DGA]+$");
        m = pAction.matcher(line3);
        m.find();

        String actions1 = m.group(0);

        //Extract position and oriontation of secode mower.
        String line4 = fileIt.next();
        m = pPosition.matcher(line4);
        m.find();

        int positionX2 = Integer.valueOf(m.group(1));
        int positionY2 = Integer.valueOf(m.group(2));
        Orientation orent2 = orientationFromString(m.group(3));

        //Extract actions of first mower.
        String line5 = fileIt.next();
        m = pAction.matcher(line3);
        m.find();

        String actions2 = m.group(0);

        //Step2 init lawn mower and execute actions.
        Lawn lawn = new Lawn(width,length);

        //Mower 1
        Mower myMower1 = new Mower();
        //set position and orientation
        myMower1.setPosition(new Position(positionX1, positionY1));
        myMower1.setOrientation(orent1);
        myMower1.putOn(lawn);

        //Execute actions
        myMower1.execute(actions1);

        //Print position
        myMower1.printPosition();

        //Mower 2
        Mower myMower2 = new Mower();
        //set position and orientation
        myMower2.setPosition(new Position(positionY2, positionY2));
        myMower2.setOrientation(orent2);
        myMower2.putOn(lawn);

        //Execute actions
        myMower2.execute(actions2);

        //Print position
        myMower2.printPosition();

    }

    private static Orientation orientationFromString(String str){
        Orientation tmp = null;
        switch(str) {
            case "N":
                tmp = Orientation.N;
                break;
            case "W":
                tmp = Orientation.W;
                break;
            case "S":
                tmp = Orientation.S;
                break;
            case "E":
                tmp = Orientation.E;
                break;
        }
        return tmp;
    }
}
